package fr.uge.reddigm.service;

import fr.uge.reddigm.dao.CommentDAO;
import fr.uge.reddigm.dao.SubjectDAO;
import fr.uge.reddigm.model.*;
import fr.uge.reddigm.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.Ordered;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Timestamp;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class UserServiceTest {
    private MockMvc mvc;
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    SubjectService subjectService;
    @Autowired
    CommentService commentService;
    private static String CATEGORY_TITLE = "Category1";
    private Category category;
    @Autowired
    private CategoryService categoryService;

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        category = categoryService.addEntity(new Category(CATEGORY_TITLE));
    }

    @Test
    void whenGoodCredentials_LoginIsSuccessful() throws Exception {
        User user = new User("test22", "$2a$10$/fAVkMGa6gHeM6Q0VHJyFOTznAo3qrXbj5CF0ZvMhxVldxYbxnk/6", Role.ROLE_USER, "test22@gmail.com");
        userRepo.save(user);
        mvc.perform(formLogin("/login").user("test22").password("spring")).andExpect(authenticated());
    }

    @Test
    void whenBadCredentials_LoginFail() throws Exception {
        User user = new User("test2", "$2a$10$/fAVkMGa6gHeM6Q0VHJyFOTznAo3qrXbj5CF0ZvMhxVldxYbxnk/6", Role.ROLE_USER, "test2@gmail.com");
        userRepo.save(user);

        mvc.perform(formLogin("/login").user("test2").password("badpassword")).andExpect(unauthenticated());
    }

    @Test
    public void shouldUpdatePassword() {
        User user = new User("test3", "$2a$10$/fAVkMGa6gHeM6Q0VHJyFOTznAo3qrXbj5CF0ZvMhxVldxYbxnk/6", Role.ROLE_USER, "test3@gmail.com");
        User usert = userService.addEntity(user);
        userService.addEntity(usert);

        String newPassword = "testP";
        userService.updatePasswordWithUser(usert, newPassword);

        assertTrue(passwordEncoder.matches(newPassword, userRepo.findByLoginEquals("test3").get().getPassword()));
    }

    @Test
    public void shouldAddUser() {
        User user = new User("test4", "helloReddigm", Role.ROLE_USER, "test4@gmail.com");
        User usert = userService.addEntity(user);
        assertTrue(userService.checkIfUserExist(usert));
    }

    @Test
    public void shouldMatchPasswordOrFail() {
        //before
        User user = new User("test5", "$2a$10$/fAVkMGa6gHeM6Q0VHJyFOTznAo3qrXbj5CF0ZvMhxVldxYbxnk/6", Role.ROLE_USER, "test5@gmail.com");
        User usert = userService.addEntity(user);
        userService.addEntity(usert);
        String newPassword = "testP";
        userService.updatePasswordWithUser(usert, newPassword);
        assertTrue(userService.matchPassword(userRepo.findByLoginEquals("test5").get().getPassword(), newPassword));
        assertFalse(userService.matchPassword(userRepo.findByLoginEquals("test5").get().getPassword(), newPassword + "t"));
    }

    @Disabled // à mocker si on a le temps
    @Test
    public void shouldFindEmail() {
        User user = new User("test6", "$2a$10$/fAVkMGa6gHeM6Q0VHJyFOTznAo3qrXbj5CF0ZvMhxVldxYbxnk/6", Role.ROLE_USER, "test6@gmail.com");
        User usert = userService.addEntity(user);
        userService.addEntity(usert);

        String email = "test6@gmail.com";
        var result = userService.checkAndSendRandomPassword(email);

        assertTrue(result);
    }

    @Disabled // à mocker si on a le temps
    @Test
    public void findEmailOnDbShouldFail() {
        User user = new User("test7", "$2a$10$/fAVkMGa6gHeM6Q0VHJyFOTznAo3qrXbj5CF0ZvMhxVldxYbxnk/6", Role.ROLE_USER, "test7@gmail.com");
        User usert = userService.addEntity(user);
        userService.addEntity(usert);

        String email = "false@gmail.com";
        var result = userService.checkAndSendRandomPassword(email);

        assertFalse(result);
    }

    @Test
    @Order(Ordered.LOWEST_PRECEDENCE)
    public void shouldFailWhenMorethan10WrongPasswordEntered() throws Exception {
        User user = new User("testForce", "password", Role.ROLE_USER, "testforcedsds@gmail.com");
        userService.addEntity(user);

        for (int i = 0; i < 10; i++) {
            mvc.perform(formLogin("/login").user("testForce").password("badpassword"));
        }
        mvc.perform(formLogin("/login").user("testForce").password("password")).andExpect(unauthenticated());
    }


    @Test
    public void ShouldFindUserSubjects() throws Exception {
        var generatedString = RandomStringUtils.randomAlphabetic(10);

        User creator = userService.addEntity(new User(generatedString, "fdfdfd", Role.ROLE_USER, generatedString.toLowerCase() + "@gmail.com"));
        int nbTour = 5;
        for (int i = 0; i < nbTour; i++) {
            var name = RandomStringUtils.randomAlphabetic(10) + ThreadLocalRandom.current().nextInt(1000);
            subjectService.addEntity(new SubjectDAO(name+" subject","content",creator.getLogin(),"Category1",new Timestamp(System.currentTimeMillis())));
        }
        var subjects = userService.getUserSubjects(creator.getId());
        assertTrue (subjects.size() == nbTour);
    }
    @Test
    public void ShouldFindUserComments() throws Exception {
        var generatedString = RandomStringUtils.randomAlphabetic(10);
        User creator = userService.addEntity(new User(generatedString, "fdfdfd", Role.ROLE_USER, generatedString.toLowerCase() + "@gmail.com"));

        var subjectDao = new SubjectDAO("titre", "hello world", creator.getLogin(), "Category1", new Timestamp(System.currentTimeMillis()));
        var subject = subjectService.addEntity(subjectDao);

        int nbTour = 5;
        for (int i = 0; i < nbTour; i++) {
            CommentDAO commentDao = new CommentDAO(subject.getId(), creator.getLogin(),"content comment", TypeContent.SUBJECT);
            Comment comment1 = commentService.addEntity(commentDao);
            CommentDAO commentDao2 = new CommentDAO(comment1.getId(), creator.getLogin(),"content comment", TypeContent.SUBJECT);

            Comment comment2 = commentService.addResponseToComment( commentDao2).get();
            CommentDAO commentDao3 = new CommentDAO(comment2.getId(), creator.getLogin(),"content comment", TypeContent.SUBJECT);
            Comment comment3 = commentService.addResponseToComment( commentDao3).get();
            CommentDAO commentDao4 = new CommentDAO(comment3.getId(), creator.getLogin(),"content comment", TypeContent.SUBJECT);
            Comment comment4 = commentService.addResponseToComment(commentDao4).get();
        }
        var comments = userService.getUserComments(creator.getId());
        Assertions.assertEquals(4*nbTour,comments.size());
    }

}
