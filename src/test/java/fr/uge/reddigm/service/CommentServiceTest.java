package fr.uge.reddigm.service;

import fr.uge.reddigm.dao.CommentDAO;
import fr.uge.reddigm.dao.SubjectDAO;
import fr.uge.reddigm.model.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@SpringBootTest
public class CommentServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubjectService subjectService;


    private MockMvc mvc;
    @Autowired
    private WebApplicationContext context;

    private static String CATEGORY_TITLE = "test";
    private static String SUBJECT_TITLE = "test";

    private Subject subject;
    private Subject subject2;
    private Category category;


    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        User user1 = userService.addEntity(new User("user1", "helloReddigm", Role.ROLE_USER,"comment1@gmail.com"));
        User user2 = userService.addEntity(new User("user2", "helloReddigm", Role.ROLE_USER,"comment2@gmail.com"));
        User user3 = userService.addEntity(new User("user3", "helloReddigm", Role.ROLE_ADMIN,"comment3@gmail.com"));

        category = categoryService.addEntity(new Category(CATEGORY_TITLE));

        SubjectDAO dao1 = new SubjectDAO(SUBJECT_TITLE, "test", "user1", CATEGORY_TITLE, new Timestamp(System.currentTimeMillis()));
        subject = subjectService.addEntity(dao1);

        SubjectDAO dao2 = new SubjectDAO(SUBJECT_TITLE + 2, "test", "user3", CATEGORY_TITLE, new Timestamp(System.currentTimeMillis()));
        subject2 = subjectService.addEntity(dao1);
    }

    @Test
    public void UnVoteCommentMustWorks() {

        User creator = userService.findUser("user1");
        var comment = commentService.addEntity(new CommentDAO(subject.getId(), creator.getLogin(),"text comment",TypeContent.SUBJECT));
        User voteur = userService.findUser("user2");
        voteur = userService.addEntity(voteur);

        commentService.downVote(voteur.getId(), comment.getId());
        commentService.downVote(voteur.getId(), comment.getId());

        org.assertj.core.api.Assertions.assertThat(commentService.getNbVotes(comment.getId())).isEqualTo(0);

    }

    @Test
    public void UpVoteCommentMustWorks() {

        User creator = userService.findUser("user1");
        var comment = commentService.addEntity(new CommentDAO(subject.getId(), creator.getLogin(),"text comment",TypeContent.SUBJECT));

        User voteur =  userService.findUser("user2");
        User voteur2 = userService.findUser("user3");
        commentService.upVote(voteur.getId(), comment.getId());
        commentService.upVote(voteur2.getId(), comment.getId());

        org.assertj.core.api.Assertions.assertThat(commentService.getNbVotes(comment.getId())).isEqualTo(2);
    }

    @Test
    public void SwitchVoteMustWorks() {

        User creator = userService.findUser("user1");
        var comment = commentService.addEntity(new CommentDAO(subject.getId(),creator.getLogin(),"text comment",TypeContent.SUBJECT));

        User vote1 = userService.findUser("user2");
        vote1 = userService.addEntity(vote1);

        commentService.upVote(vote1.getId(), comment.getId());
        commentService.downVote(vote1.getId(), comment.getId());

        org.assertj.core.api.Assertions.assertThat(commentService.getNbVotes(comment.getId())).isEqualTo(-1);
    }
    @Test
    public void MassiveVoteMustWorks() throws Exception {
        var generatedString = RandomStringUtils.randomAlphabetic(10);
        var threads = new ArrayList<Thread>();
        User creator = userService.addEntity(new User(generatedString, "fdfdfd", Role.ROLE_USER, generatedString.toLowerCase() + "@gmail.com"));
        var comment = commentService.addEntity(new CommentDAO(subject.getId(),creator.getLogin(),"text comment",TypeContent.SUBJECT));

        for (int i = 0; i < 10; i++) {
            var thread = new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    var name = RandomStringUtils.randomAlphabetic(10) + ThreadLocalRandom.current().nextInt(1000);
                    var voteur = userService.addEntity(new User(name, "fdfdfd", Role.ROLE_USER, name.toLowerCase() + "@gmail.com"));
                    commentService.downVote(voteur.getId(), comment.getId());

                }
            });
            threads.add(thread);
            thread.start();
        }
        for (var thread : threads) {
            thread.join();
        }

        org.assertj.core.api.Assertions.assertThat(commentService.getNbVotes(comment.getId())).isEqualTo(-100);
    }

    @Test
    public void shouldAddCommentToSubject() {
        User author = userService.findUser("user1");

        CommentDAO commentDao = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        CommentDAO commentDao2 = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment = commentService.addEntity(commentDao);
        commentService.addEntity(commentDao2);

        Assertions.assertTrue(commentService.retrieveComment(comment.getId()).isPresent());

    }

    @Test
    public void subjectShouldHave2Comments() {
        User author = userService.findUser("user1");
        User user = userService.findUser("user2");

        CommentDAO commentDao = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        CommentDAO commentDao2 = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment = commentService.addEntity(commentDao);
        commentService.addEntity(commentDao2);

        Assertions.assertTrue(commentService.retrieveComment(comment.getId()).isPresent());
    }

    @Test
    public void commentShouldHave3Comments() {
        User author = userService.findUser("user1");
        User user = userService.findUser("user2");
        User admin = userService.findUser("user3");

        CommentDAO commentDao = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);

        Comment comment1 = commentService.addEntity(commentDao);
        CommentDAO commentDao2 = new CommentDAO(comment1.getId(), user.getLogin(),"content comment",TypeContent.COMMENT);
        CommentDAO commentDao3 = new CommentDAO(comment1.getId(), author.getLogin(),"content comment",TypeContent.COMMENT);
        CommentDAO commentDao4 = new CommentDAO(comment1.getId(), admin.getLogin(),"content comment",TypeContent.COMMENT);
        Comment comment2 = commentService.addResponseToComment( commentDao2).get();
        Comment comment3 = commentService.addResponseToComment( commentDao3).get();
        Comment comment4 = commentService.addResponseToComment( commentDao4).get();

        comment1 = commentService.retrieveCommentWithWithCommentsAndAuthor(comment1.getId()).get();
        assertTrue(comment1.getComments().size() == 3);
    }

    @Test
    public void commentShouldHave3CommentsWithNestedComment() {
        User author = userService.findUser("user1");
        User user = userService.findUser("user2");
        User admin = userService.findUser("user3");

        CommentDAO commentDao = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);

        Comment comment1 = commentService.addEntity(commentDao);
        CommentDAO commentDao2 = new CommentDAO(comment1.getId(), user.getLogin(),"content comment",TypeContent.COMMENT);
        Comment comment2 = commentService.addResponseToComment( commentDao2).get();
        CommentDAO commentDao3 = new CommentDAO(comment2.getId(), author.getLogin(),"content comment",TypeContent.COMMENT);
        commentService.addResponseToComment(commentDao3).get();
        CommentDAO commentDao4 = new CommentDAO(comment1.getId(), admin.getLogin(),"content comment",TypeContent.COMMENT);
        commentService.addResponseToComment(commentDao4).get();
        comment1 = commentService.retrieveCommentWithWithCommentsAndAuthor(comment1.getId()).get();
        comment2 = commentService.retrieveCommentWithWithCommentsAndAuthor(comment2.getId()).get();
        var updatedsubject = subjectService.retrieveSubjectById(subject.getId());
        assertTrue(comment1.getComments().size() == 3);
        assertTrue(comment2.getComments().size() == 3);
    }

    @Test
    public void shouldDeleteCommentWhenAdmin() {
        User author = userService.findUser("user1");

        CommentDAO commentDao = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment = commentService.addEntity(commentDao);

        commentService.addResponseToComment(new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.COMMENT));

        long commentId = comment.getId();

        assertTrue(commentService.deleteEntity(commentId,author.getRole(),commentDao.getContentId(),author.getLogin()));
        assertTrue(commentService.retrieveComment(commentId).isEmpty());
    }


    @Test
    public void shouldDeleteCommentWhenOwnerComment() {
        User author = userService.findUser("user1");
        CommentDAO commentDao = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment = commentService.addEntity(commentDao);
        assertTrue(commentService.deleteEntity(comment.getId(),author.getRole(),commentDao.getContentId(),author.getLogin()));// check later !!
    }


    @Test
    public void shouldFailDeleteCommentWhenNotAdminAndNotowner() {
        User author = userService.findUser("user1");
        User user = userService.findUser("user2");

        CommentDAO commentDao = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment = commentService.addEntity(commentDao);

        assertFalse(commentService.deleteEntity(comment.getId(),user.getRole(),commentDao.getContentId(),user.getLogin()));
    }

    @Test
    public void shouldRespondToComment() {
        User author = userService.findUser("user1");
        User user = userService.findUser("user2");
        CommentDAO commentDao1 = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment1 = commentService.addEntity(commentDao1);
        CommentDAO commentDao2 = new CommentDAO(comment1.getId(), user.getLogin(),"content comment",TypeContent.COMMENT);
        assertTrue(commentService.addResponseToComment( commentDao2).isPresent());
    }

    @Test
    public void shouldRespondThemself() {
        User author = userService.findUser("user1");

        CommentDAO commentDao1 = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment1 = commentService.addEntity(commentDao1);
        CommentDAO commentDao2 = new CommentDAO(comment1.getId(), author.getLogin(),"content comment",TypeContent.COMMENT);

        assertTrue(commentService.addResponseToComment(commentDao2).isPresent());
    }

    @Test
    public void shouldNotRespondToDeletedComment() {
        User author = userService.findUser("user1");
        User user = userService.findUser("user2");
        CommentDAO commentDao1 = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment1 = commentService.addEntity(commentDao1);
        assertTrue(commentService.deleteEntity(comment1.getId(), author.getRole(),commentDao1.getContentId(),author.getLogin()));
        CommentDAO commentDao2 = new CommentDAO(subject.getId(), user.getLogin(),"content comment",TypeContent.SUBJECT);
        assertFalse(commentService.addResponseToComment( commentDao2).isPresent());
    }

    @Test
    public void shouldDeleteCommentAndItsResponses() {
        User author = userService.findUser("user1");
        User user = userService.findUser("user2");

        CommentDAO commentDao1 = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment1 = commentService.addEntity(commentDao1);
        CommentDAO commentDao2 = new CommentDAO(comment1.getId(), user.getLogin(),"content comment",TypeContent.COMMENT);
        Optional<Comment> oc2 = commentService.addResponseToComment( commentDao2);
        Comment comment2 = oc2.get();
        CommentDAO commentDao3 = new CommentDAO(comment2.getId(), user.getLogin(),"content comment",TypeContent.COMMENT);
        Optional<Comment> oc3 = commentService.addResponseToComment( commentDao3);
        Comment comment3 = oc3.get();

        assertTrue(commentService.deleteEntity(comment1.getId(), author.getRole(), commentDao1.getContentId(),author.getLogin()));
        assertFalse(commentService.retrieveComment(comment1.getId()).isPresent());
        assertFalse(commentService.retrieveComment(comment2.getId()).isPresent());
        assertFalse(commentService.retrieveComment(comment3.getId()).isPresent());
    }


    @Test
    public void shouldUpdateComment(){
        User author = userService.findUser("user1");
        CommentDAO commentDao1 = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment1 = commentService.addEntity(commentDao1);
        CommentDAO commentDao2 = new CommentDAO(subject.getId(), comment1.getAuthor().getLogin(), "content comment modified",TypeContent.COMMENT);
        Optional<Comment> result = commentService.update(comment1.getId(), commentDao2);
        assertTrue(result.isPresent());
    }

    @Test
    public void shouldntUpdateAnOtherUserComment(){
        User author = userService.findUser("user1");
        User user = userService.findUser("user2");
        CommentDAO commentDao1 = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment1 = commentService.addEntity(commentDao1);
        CommentDAO commentDao2 = new CommentDAO(subject.getId(), user.getLogin(), "content comment modified",TypeContent.COMMENT);
        Optional<Comment> oComment2 = commentService.update(comment1.getId(), commentDao2);
        assertTrue(oComment2.isEmpty());
    }

    @Test
    public void shouldntUpdateWithWrongSubject(){
        User author = userService.findUser("user1");
        User user = userService.findUser("user2");
        CommentDAO commentDao1 = new CommentDAO(subject.getId(), author.getLogin(),"content comment",TypeContent.SUBJECT);
        Comment comment1 = commentService.addEntity(commentDao1);
        CommentDAO commentDao2 = new CommentDAO(subject2.getId(), user.getLogin(), "content comment modified",TypeContent.COMMENT);
        Optional<Comment> oComment2 = commentService.update(comment1.getId(), commentDao2);
        assertTrue(oComment2.isEmpty());
    }

}
