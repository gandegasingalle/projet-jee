package fr.uge.reddigm.service;

import fr.uge.reddigm.dao.SubjectDAO;
import fr.uge.reddigm.model.*;
import fr.uge.reddigm.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@SpringBootTest
public class SubjectServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;
    @Autowired
    private UserRepository userRepo;

    @Autowired
    SubjectService subjectService;
    private MockMvc mvc;
    @Autowired
    private WebApplicationContext context;


    @Autowired
    private CategoryService categoryService;

    private static String CATEGORY_TITLE = "Category1";
    private Category category;



    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        category = categoryService.addEntity(new Category(CATEGORY_TITLE));
    }


    @Test
    public void upvoteMustWorksWhenUnvoting() throws IOException {

        User creator = userService.addEntity(new User("creator", "helloReddigm", Role.ROLE_USER, "vote1@gmail.com"));
        var subjectDao = new SubjectDAO("titre", "hello world", creator.getLogin(), category.getTitle(), new Timestamp(System.currentTimeMillis()));
        var subject = subjectService.addEntity(subjectDao);

        User vote1 = new User("vote1", "helloReddigm", Role.ROLE_USER, "vote10@gmail.com");
        vote1 = userService.addEntity(vote1);
        User vote2 = new User("vote2", "helloReddigm", Role.ROLE_USER, "vote11@gmail.com");
        vote2 = userService.addEntity(vote2);


        subjectService.upVote(vote1.getId(), subject.getId());
        subjectService.upVote(vote2.getId(), subject.getId());
        subjectService.upVote(vote2.getId(), subject.getId());//unvote
        subjectService.upVote(vote2.getId(), subject.getId());//vote
        subjectService.upVote(vote2.getId(), subject.getId());//unvote


        org.assertj.core.api.Assertions.assertThat(subjectService.getNbVotes(subject.getId())).isEqualTo(1);

    }


    @Test
    public void upvoteAndDownVoteMustWorks() throws IOException {

        User creator = userService.addEntity(new User("creator2", "helloReddigm", Role.ROLE_USER, "vote2@gmail.com"));
        var subjectDao = new SubjectDAO("titre", "hello world", creator.getLogin(), category.getTitle(), new Timestamp(System.currentTimeMillis()));
        var subject = subjectService.addEntity(subjectDao);

        User vote1 = new User("vote1", "helloReddigm", Role.ROLE_USER, "vote7@gmail.com");
        vote1 = userService.addEntity(vote1);
        User vote2 = new User("vote2", "helloReddigm", Role.ROLE_USER, "vote8@gmail.com");
        vote2 = userService.addEntity(vote2);

        subjectService.upVote(vote1.getId(), subject.getId());
        subjectService.downVote(vote2.getId(), subject.getId());

        org.assertj.core.api.Assertions.assertThat(subjectService.getNbVotes(subject.getId())).isEqualTo(0);

    }

    @Test
    public void UnVoteMustWorks() throws IOException {

        User creator = userService.addEntity(new User("creator3", "helloReddigm", Role.ROLE_USER, "vote3@gmail.com"));
        var subjectDao = new SubjectDAO("titre", "hello world", creator.getLogin(), category.getTitle(), new Timestamp(System.currentTimeMillis()));
        var subject = subjectService.addEntity(subjectDao);

        User vote2 = new User("vote2", "helloReddigm", Role.ROLE_USER, "vote6@gmail.com");
        vote2 = userService.addEntity(vote2);

        subjectService.downVote(vote2.getId(), subject.getId());
        subjectService.downVote(vote2.getId(), subject.getId());

        org.assertj.core.api.Assertions.assertThat(subjectService.getNbVotes(subject.getId())).isEqualTo(0);

    }


    @Test
    public void SwitchVoteMustWorks() throws IOException {

        User creator = userService.addEntity(new User("creator4", "helloReddigm", Role.ROLE_USER, "vote4@gmail.com"));
        var subjectDao = new SubjectDAO("titre", "hello world", creator.getLogin(), category.getTitle(), new Timestamp(System.currentTimeMillis()));
        var subject = subjectService.addEntity(subjectDao);

        User vote1 = new User("vote1", "helloReddigm", Role.ROLE_USER, "vote5@gmail.com");
        vote1 = userService.addEntity(vote1);
        User vote2 = new User("vote2", "helloReddigm", Role.ROLE_USER, "vote6@gmail.com");
        vote2 = userService.addEntity(vote2);

        subjectService.upVote(vote1.getId(), subject.getId());
        subjectService.downVote(vote1.getId(), subject.getId());

        org.assertj.core.api.Assertions.assertThat(subjectService.getNbVotes(subject.getId())).isEqualTo(-1);
    }


    @Test
    public void MassiveVoteMustWorks() throws Exception {
        var generatedString = RandomStringUtils.randomAlphabetic(10);
        var threads = new ArrayList<Thread>();

        User creator = userService.addEntity(new User(generatedString, "fdfdfd", Role.ROLE_USER, generatedString.toLowerCase() + "@gmail.com"));
        var subjectDao = new SubjectDAO("titre", "hello world", creator.getLogin(), category.getTitle(), new Timestamp(System.currentTimeMillis()));
        var subject = subjectService.addEntity(subjectDao);
        var subjectDao2 = new SubjectDAO("titre", "hello world", creator.getLogin(), category.getTitle(), new Timestamp(System.currentTimeMillis()));
        var subject2 = subjectService.addEntity(subjectDao);

        for (int i = 0; i < 10; i++) {
            var thread = new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    var name = RandomStringUtils.randomAlphabetic(10) + ThreadLocalRandom.current().nextInt(1000);
                    var voteur = userService.addEntity(new User(name, "fdfdfd", Role.ROLE_USER, name.toLowerCase() + "@gmail.com"));
                    subjectService.downVote(voteur.getId(), subject.getId());
                    subjectService.upVote(voteur.getId(), subject2.getId());
                }
            });
            threads.add(thread);
            thread.start();
        }
        for (var thread : threads) {
            thread.join();
        }

        org.assertj.core.api.Assertions.assertThat(subjectService.getNbVotes(subject.getId())).isEqualTo(-10*10);
        org.assertj.core.api.Assertions.assertThat(subjectService.getNbVotes(subject2.getId())).isEqualTo(10*10);
    }

    @Test
    public void shouldDeleteSubjectWhenAdmin() throws IOException {
        User admin = userRepo.save( new User("admin1", "admin", Role.ROLE_ADMIN,"admin1@gmail.com"));
        Category category = categoryService.addEntity(new Category("category test"));
        SubjectDAO subjectDAO = new SubjectDAO("title","content", admin.getLogin(), category.getTitle(), new Timestamp(100l));

        Subject subject = subjectService.addEntity(subjectDAO);

        assertTrue(subjectService.deleteSubject(subject.getId(),admin.getRole()));

    }

    @Test
    public void shouldFailDeleteSubjectWhenNotAdmin() throws IOException {
        User user = userService.addEntity( new User("user2", "admin", Role.ROLE_USER,"user2@gmail.com"));
        Category category = categoryService.addEntity(new Category("category test"));
        SubjectDAO subjectDAO = new SubjectDAO("title","content", user.getLogin(), category.getTitle(), new Timestamp(100l));

        Subject subject = subjectService.addEntity(subjectDAO);

        assertFalse(subjectService.deleteSubject(subject.getId(),user.getRole()));

    }

    @Test
    public void shouldDeleteVotesDeletedSubject() throws IOException {
        User admin = userRepo.save( new User("admin2", "admin", Role.ROLE_ADMIN,"admin2@gmail.com"));
        User user = userService.addEntity( new User("user", "admin", Role.ROLE_USER,"user@gmail.com"));
        Category category = categoryService.addEntity(new Category("category test"));
        SubjectDAO subjectDAO = new SubjectDAO("title","content", admin.getLogin(), category.getTitle(), new Timestamp(100l));

        Subject subject = subjectService.addEntity(subjectDAO);
        subjectService.upVote(user.getId(), subject.getId());
        subjectService.deleteSubject(subject.getId(),admin.getRole());

        assertThrows(IllegalStateException.class,() -> subjectService.getNbVotes(user.getId()));

    }

    @Test
    public void addSubjectWithEmptyFieldsShouldFail(){
        User user = new User("test", "$2a$10$/fAVkMGa6gHeM6Q0VHJyFOTznAo3qrXbj5CF0ZvMhxVldxYbxnk/6", Role.ROLE_USER,"test@gmail.com");
        userService.addEntity(user);

        SubjectDAO dao = new SubjectDAO("", "", "test", "Category1",new Timestamp(System.currentTimeMillis()));
        assertThrows(TransactionSystemException.class, () -> subjectService.addEntity(dao));
    }
}
