package fr.uge.reddigm.retry;

import fr.uge.reddigm.Utils.VoteType;
import fr.uge.reddigm.model.Vote;
import fr.uge.reddigm.repository.SubjectRepository;
import fr.uge.reddigm.repository.UserRepository;
import fr.uge.reddigm.repository.VotableRepository;
import fr.uge.reddigm.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class VotableServiceWithFailure {
    @Autowired
    SubjectRepository subjectRepo;
    @Autowired
    UserRepository userRepository;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private VotableRepository votableRepository;


    @Transactional
    public void addVoteWrong(Long userId, Long subjectId, VoteType voteType) throws ObjectOptimisticLockingFailureException {
        var user = userRepository.findById(userId).get();
        var subject = votableRepository.findById(subjectId).get();

        if (user != null && subject != null && userId != subject.getAuthor().getId()) {

            var vote = voteRepository.findByVotableAndUser(subjectId, userId);

            if (vote != null) {
                if (vote.getType() == voteType) { //unVote
                    subject.removeVote(vote);
                } else {//switch vote
                    subject.removeVote(vote);
                    vote.setType(voteType);
                    subject.addVote(vote);
                }
            } else {
                subject.addVote(new Vote(user, subject, voteType));
            }
        }
        votableRepository.save(subject);
    }

}
