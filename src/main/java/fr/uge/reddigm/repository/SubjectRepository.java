package fr.uge.reddigm.repository;

import fr.uge.reddigm.model.Subject;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface SubjectRepository extends CrudRepository<Subject, Long> {

    @Query(value = "SELECT s FROM Subject s LEFT JOIN FETCH s.author u LEFT JOIN FETCH s.category c ")
    List<Subject> findAll();

    //Subject deleteSubjectByAuthor_LoginAndCategoryAndTitleAndTime(String author, String category, String Title, Timestamp time);
    @Query(value = "SELECT s FROM Subject s LEFT JOIN FETCH s.comments ct LEFT JOIN FETCH ct.author WHERE s.id=:ids ")
    Optional<Subject> findSubjectWithComments(@Param("ids") long id);

    @Query(value = "SELECT s FROM Subject s LEFT JOIN FETCH s.author ct  WHERE s.id=:ids")
    Optional<Subject> findSubjectWithId(@Param("ids") long id);

    @Query(value = "SELECT s FROM Subject s LEFT JOIN FETCH s.category c LEFT JOIN FETCH s.author  WHERE s.id=:ids ")
    Optional<Subject> retrieveSubjectByIdForUpdate(@Param("ids") long id);


}
