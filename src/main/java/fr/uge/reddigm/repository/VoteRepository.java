package fr.uge.reddigm.repository;

import fr.uge.reddigm.model.Votable;
import fr.uge.reddigm.model.Vote;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface VoteRepository extends CrudRepository<Vote, Long> {

    @Query("SELECT votes FROM Votable s join s.votes votes WHERE s.id = ?1 and votes.user.id = ?2")
    Vote findByVotableAndUser(Long subjectDd, Long userId);

    @Query(value = "SELECT c FROM Votable c WHERE c.id=:ids")
    Optional<Votable> findContentWithId(@Param("ids")long id);


}
