package fr.uge.reddigm.repository;

import fr.uge.reddigm.model.Votable;
import org.springframework.data.repository.CrudRepository;

public interface VotableRepository extends CrudRepository<Votable, Long> {
}
