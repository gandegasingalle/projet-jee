package fr.uge.reddigm.repository;

import fr.uge.reddigm.model.Comment;
import fr.uge.reddigm.model.Votable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CommentRepository extends CrudRepository<Comment, Long> {

    @Query(value = "SELECT c FROM Comment c LEFT JOIN FETCH c.comments ct WHERE c.id=:ids ")
    Optional<Comment> findByIdWithComments(@Param("ids") Long ids);

    @Query(value = "SELECT c FROM Comment c  LEFT JOIN FETCH c.author ct LEFT join FETCH c.comments WHERE c.id=:ids ")
    Optional<Comment> findByIdWithCommentsAndAuthor(@Param("ids") Long ids);





}
