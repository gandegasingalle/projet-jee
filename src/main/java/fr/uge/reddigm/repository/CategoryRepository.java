package fr.uge.reddigm.repository;

import fr.uge.reddigm.model.Category;
import fr.uge.reddigm.model.Subject;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {

    Optional<Category> findByTitle(String title);

    @Query(value = "SELECT DISTINCT c FROM Category c LEFT JOIN FETCH c.subjects s")
    List<Category> findAll();

}
