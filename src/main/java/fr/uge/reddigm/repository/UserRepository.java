package fr.uge.reddigm.repository;

import fr.uge.reddigm.model.Comment;
import fr.uge.reddigm.model.Subject;
import fr.uge.reddigm.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByLoginEquals(String login);

    Optional<User> findByEmail(String email);

    @Query(value = "SELECT s FROM Subject s LEFT JOIN FETCH s.author u LEFT JOIN FETCH s.category c WHERE u.id=:ids ")
    List<Subject> findUserSubjects(@Param("ids") Long ids);

    @Query(value = "SELECT c FROM Comment c  WHERE c.author.id=:ids ")
    List<Comment> findUserComments(@Param("ids") Long ids);
}
