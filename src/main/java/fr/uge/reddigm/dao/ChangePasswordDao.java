package fr.uge.reddigm.dao;

public class ChangePasswordDao {
    private String oldPassword;
    private String newPassword;
    private String confirmPassword;

    public ChangePasswordDao(String oldPassword, String newPassword, String confirmPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.confirmPassword = confirmPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public boolean TryChangePassword(){
        return  !newPassword.isBlank() && newPassword.equals(confirmPassword);
    }

}
