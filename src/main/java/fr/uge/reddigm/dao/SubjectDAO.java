package fr.uge.reddigm.dao;

import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

public class SubjectDAO {
    @NotEmpty
    private String title;
    @NotEmpty
    private String content;
    private String author;
    @NotEmpty
    private String category;
    private Timestamp time;

    public SubjectDAO() {
    }

    public SubjectDAO(String title, String content, String author, String category, Timestamp time) {
        this.title = title;
        this.content = content;
        this.author = author;
        this.category = category;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "SubjectDAO{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", author='" + author + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
