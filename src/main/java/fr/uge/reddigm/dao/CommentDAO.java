package fr.uge.reddigm.dao;


import fr.uge.reddigm.model.TypeContent;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

public class CommentDAO {

    private Long contentId;
    @NotEmpty
    private String author;
    @NotEmpty
    private String content;
    private TypeContent isSubjectOrComment;


    public CommentDAO(Long contentId, String author, String content, TypeContent isSubjectOrComment) {
        this.contentId = contentId;
        this.author = author;
        this.content = content;
        this.isSubjectOrComment = isSubjectOrComment;
    }

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public TypeContent isSubjectOrComment() {
        return isSubjectOrComment;
    }

    public void setSubjectOrComment(TypeContent subjectOrComment) {
        isSubjectOrComment = subjectOrComment;
    }


}
