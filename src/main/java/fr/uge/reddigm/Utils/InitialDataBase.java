package fr.uge.reddigm.Utils;

import fr.uge.reddigm.dao.CommentDAO;
import fr.uge.reddigm.model.*;
import fr.uge.reddigm.repository.CategoryRepository;
import fr.uge.reddigm.repository.CommentRepository;
import fr.uge.reddigm.repository.SubjectRepository;
import fr.uge.reddigm.repository.UserRepository;
import fr.uge.reddigm.service.CommentService;
import fr.uge.reddigm.service.SubjectService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Base64;
import java.util.HashSet;

@Component
public class InitialDataBase {
    @Autowired private SubjectRepository subjectRepository;
    @Autowired private CategoryRepository categoryRepository;
    @Autowired private UserRepository userRepository;
    @Autowired private PasswordEncoder passwordEncoder;
    @Autowired private SubjectService subjectService;
    @Autowired private CommentService commentService;
    @Autowired private CommentRepository commentRepository;


String contentTest =  "<p>L'informatique est un domaine d'activité scientifique, technique, et industriel concernant le traitement automatique de l'information numérique par l'exécution de programmes informatiques hébergés par des dispositifs électriques-électroniques : des systèmes embarqués, des ordinateurs, des robots, des automates, etc.\n" +
        "\n" +
        "Ces champs d'application peuvent être séparés en deux branches :\n" +
        "\n" +
        "théorique : concerne la définition de concepts et modèles ;\n" +
        "pratique : s'intéresse aux techniques concrètes de mise en œuvre.\n" +
        "Certains domaines de l'informatique peuvent être très abstraits, comme la complexité algorithmique, et d'autres peuvent être plus proches d'un public profane. Ainsi, la théorie des langages demeure un domaine davantage accessible aux professionnels formés (description des ordinateurs et méthodes de programmation), tandis que les métiers liés aux interfaces homme-machine sont accessibles à un plus large public.</p>";
    @Bean
    public CommandLineRunner cmd(){

        return args -> {
            var test = new User("test", passwordEncoder.encode("spring"), Role.ROLE_USER,"test@gmail.com");
            var arnaud = new User("arnaud", passwordEncoder.encode("arnaud"), Role.ROLE_USER,"arnaud@gmail.com");
            var admin = new User("admin", passwordEncoder.encode("admin") , Role.ROLE_ADMIN,"admin@gmail.com");
            arnaud.setPhoto(Files.readAllBytes(Paths.get("Arnaud-Carayol.jpg")));
            admin.setPhoto(Files.readAllBytes(Paths.get("admin.jpg")));
            test.setPhoto(Files.readAllBytes(Paths.get("test.jpg")));

            arnaud = userRepository.save(arnaud);
            admin = userRepository.save(admin);
            test = userRepository.save(test);


            var category = new Category("Divers");
            var category2 = new Category("Foot");
            var category3 = new Category("Java");
            categoryRepository.save(category);
            categoryRepository.save(category2);
            categoryRepository.save(category3);

            byte[] fileContent1 = FileUtils.readFileToByteArray(new File("image1.jpg"));
            String encodedString1 = Base64.getEncoder().encodeToString(fileContent1);

            byte[] fileContent2 = FileUtils.readFileToByteArray(new File("image2.jpg"));
            String encodedString2 = Base64.getEncoder().encodeToString(fileContent2);

            byte[] fileContent3 = FileUtils.readFileToByteArray(new File("image3.jpg"));
            String encodedString3 = Base64.getEncoder().encodeToString(fileContent3);

            var subject = new Subject("Parlons peu parlons bien", "Je suis de bonne humeur alors je la partage avec vous ! " +
                    "\n <p><img src = data:image1/jpg;base64," + encodedString1 + " style=\"width: 400px; height: 250px;\"><br></p>\t",
                    test,category,new HashSet<>(), Timestamp.from(Instant.now()));
            var subject2 = new Subject("Parlons bien seulement", "Je suis de bonne humeur  ! <a href=\"/to?url=&quot;http://www.google.fr&quot;\" target=\"_blank\">www.google.fr</a> " +
                    "\n <p><img src = data:image2/jpg;base64," + encodedString2 + " style=\"width: 400px; height: 250px;\"><br></p>\t",
                    test,category,new HashSet<>(), Timestamp.from(Instant.now() ));

            var subject3 = new Subject("Parlons peu parlons bien", contentTest +
                    "\n <p><img src = data:image3/jpg;base64," + encodedString3 + " style=\"width: 400px; height: 250px;\"><br></p>\t",
                    test,category,new HashSet<>(), Timestamp.from(Instant.now()));

            subject = subjectRepository.save(subject);
            subject2 = subjectRepository.save(subject2);
            subject3 = subjectRepository.save(subject3);


            subjectService.upVote(admin.getId(),subject.getId());
            subjectService.downVote(admin.getId(),subject2.getId());
            subjectService.upVote(arnaud.getId(),subject.getId());

            CommentDAO commentDao = new CommentDAO(subject.getId(), admin.getLogin(),"content comment",TypeContent.SUBJECT);
            Comment comment1 = commentService.addEntity(commentDao);

            CommentDAO commentDao1Bis = new CommentDAO(subject.getId(), arnaud.getLogin(),"content comment Bis",TypeContent.SUBJECT);
            Comment comment1Bis = commentService.addEntity(commentDao1Bis);

            CommentDAO commentDao2 = new CommentDAO(comment1.getId(), test.getLogin(),"content comment",TypeContent.COMMENT);

            Comment comment2 = commentService.addResponseToComment( commentDao2).get();

            CommentDAO commentDao3 = new CommentDAO(comment2.getId(), arnaud.getLogin(),"content comment",TypeContent.COMMENT);
            var c3 = commentService.addResponseToComment(commentDao3).get();
            commentService.downVote(test.getId(), c3.getId());
            CommentDAO commentDao4 = new CommentDAO(comment1.getId(), admin.getLogin(),"content comment",TypeContent.COMMENT);
            var c4 = commentService.addResponseToComment( commentDao4).get();
            commentService.upVote(test.getId(), comment1Bis.getId());
            commentService.upVote(admin.getId(), comment1Bis.getId());

        };
    }

}
