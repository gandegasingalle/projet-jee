package fr.uge.reddigm.controllers;

import fr.uge.reddigm.dao.ChangePasswordDao;
import fr.uge.reddigm.model.Subject;
import fr.uge.reddigm.model.User;
import fr.uge.reddigm.model.UserDetails;
import fr.uge.reddigm.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private CommentService commentService;

    @GetMapping("/")
    public String index(Model model, @RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {
        model.addAttribute("categories", categoryService.retrieveCategoriesOrdered());

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Subject> subjectPage = subjectService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        model.addAttribute("subjectPage", subjectPage);

        int totalPages = subjectPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "index";
    }

    @GetMapping("/login")
    public String login(Model model) {
        boolean blocked = false;
        if (userDetailsService.isBlocked() != null) {
            blocked = userDetailsService.isBlocked();
        }
        model.addAttribute("blocked", blocked);
        return "login";
    }

    @GetMapping("/register")
    public String register() {
        return "register";
    }

    @PostMapping("/register")
    public String signUp(@Valid @ModelAttribute("register") User user, BindingResult result, Model model) {
        var existOptional = userService.checkLoginAndEmailExists(user);
        if (result.hasErrors() || existOptional.isPresent()) {
            existOptional.ifPresent(s -> {
                model.addAttribute("exist", true);
                model.addAttribute("existMessage", s);
            });
            return "registerValidation";
        }

        userService.addEntity(user);
        return "redirect:/";
    }

    @GetMapping("/changePassword")
    public String changePasswordGet() {
        return "changePassword";
    }

    @PostMapping("/changePassword")
    public String changePasswordPost(@ModelAttribute("User") ChangePasswordDao changePasswordDao, Authentication authentication, Model model) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var boolChangPass = changePasswordDao.TryChangePassword();
        model.addAttribute("checkNewPassword", !boolChangPass);

        var checked = boolChangPass && userService.matchPassword(userDetails.getPassword(), changePasswordDao.getOldPassword());
        model.addAttribute("sended", checked);
        if (checked) {
            userService.updatePasswordWithUser(userDetails.getUser(), changePasswordDao.getNewPassword());
        }
        return "changePassword";
    }


    @GetMapping(value = "/recovery")
    public String recovery(Model model) {
        model.addAttribute("emailValidation", "undefined");
        return "recovery_password";
    }

    @PostMapping(value = "/recovery")
    public String recoveryPost(String email, Model model) {
        var user = userService.checkAndSendRandomPassword(email);
        if (user) {
            model.addAttribute("emailValidation", "valid");
        } else {
            model.addAttribute("emailValidation", "invalid");
        }
        return "recovery_password";
    }

    @GetMapping(value = "/mySubjects")
    public String mySubjects(Model model, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var sub = userService.getUserSubjects(userDetails.getUserId());
        model.addAttribute("subjects", sub);
        return "userSubjects";
    }

    @GetMapping(value = "/myComments")
    public String myComments(Model model, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var comments = userService.getUserComments(userDetails.getUserId());
        model.addAttribute("comments", comments);
        model.addAttribute("commentService", commentService);
        return "userComments";
    }

    @GetMapping(value = "/profile")
    public String profile(Authentication authentication, Model model) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var user = userService.findUser(userDetails.getUsername());
        model.addAttribute("photoProfil", user.getPhoto());
        model.addAttribute("userName", user.getLogin());
        model.addAttribute("email", user.getEmail());
        return "profile";
    }

    @PostMapping(value = "/profile/updatePhoto")
    public String profile(@RequestParam("image") MultipartFile multipartFile, Model model, Authentication authentication) {
        //model.addAttribute("emailValidation","undefined");
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        byte[] image;
        if ( multipartFile.getSize() >=  5193728 ){
            model.addAttribute("message", "The size of the profile picture is limited to 5MB");
            model.addAttribute("url", "/profile");
            return "warning";
        }
        try {
            image = multipartFile.getBytes();
        } catch (IOException ex) {
            model.addAttribute("message", "can't load your image.");
            model.addAttribute("code", 500);
            return "error";
        }
        userService.updateProfileImage(userDetails.getUsername(), image);
        return "redirect:/profile";
    }

    @GetMapping(value = "/profile/changePassword")
    public String redirectChangePassword() {
        return "redirect:/changePassword";
    }

    @PostMapping(value = "/profile/email")
    public String emailUpdate(@Valid @ModelAttribute("user") User user,BindingResult result, Authentication authentication, Model model) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var userAuth = userDetails.getUser();
        model.addAttribute("photoProfil", userAuth.getPhoto());
        model.addAttribute("userName", userAuth.getLogin());
        var existOptional = userService.checkLoginAndEmailExists(user);
        if (result.hasErrors() || existOptional.isPresent()) {
            existOptional.ifPresent(s -> {
                model.addAttribute("exist", true);
                model.addAttribute("existMessage", s);
            });
            return "profileValidation";
        }
        model.addAttribute("email",user.getEmail());
        userService.updateProfileEmail(userDetails.getUsername(), user.getEmail());
        model.addAttribute("emailChanged", true);

        return "profile";
    }

    @GetMapping(value = "/to")
    public String exitWebSite(@RequestParam("url") String url, Model model) {
        model.addAttribute("lien", url.replace("\"", ""));
        return "exit";
    }
}