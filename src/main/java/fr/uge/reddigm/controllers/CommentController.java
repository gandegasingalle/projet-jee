package fr.uge.reddigm.controllers;

import fr.uge.reddigm.dao.CommentDAO;
import fr.uge.reddigm.model.TypeContent;
import fr.uge.reddigm.model.UserDetails;
import fr.uge.reddigm.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class CommentController {
    @Autowired
    private CommentService commentService;


    @PostMapping("/comment")
    public String respondToSubject(@ModelAttribute("comment") CommentDAO commentDAO,Authentication authentication) {
        if(commentDAO.getContent().isEmpty()){
            return "redirect:/";
        }
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        commentDAO.setAuthor(userDetails.getUser().getLogin());
        var contentId = commentDAO.getContentId();
        var URL = "redirect:/subjects/";
        if(commentDAO.isSubjectOrComment() == TypeContent.SUBJECT){ // add comment to subject
            commentService.addEntity(commentDAO);
            URL += contentId+"/";
        }else{                                                     // add comment to comment
            var subjectId = commentService.getSubjectIdFromComment(contentId);
            commentService.addResponseToComment(commentDAO);
            URL += subjectId+"/";
        }
        return URL;
    }



    @GetMapping(value = "/editComment/{id}")
    public String edit(Model model, @PathVariable("id") Long id, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var optionalComment = commentService.retrieveComment(id);
        if (optionalComment.isEmpty()) {
            model.addAttribute("message", "The comment you are looking for was not found.");
            model.addAttribute("code", 404);
            return "error";
        }
        var comment = optionalComment.get();
        if( comment.getAuthor().getId() != userDetails.getUserId()){
            model.addAttribute("message", "unauthorized edit");
            model.addAttribute("code", 404);
            return "error";
        }
        model.addAttribute("comment", comment);
        model.addAttribute("votableId", comment.getVotable().getId());
        return "edit-comment";
    }


    @PostMapping(value = "/updateComment/{id}")
    public String edit(@PathVariable("id") Long id,@ModelAttribute("comment") CommentDAO commentDAO,Authentication authentication) {
        var comment  = commentService.update(id,commentDAO);
        if (comment.isEmpty()){
            return "redirect:/";
        }
        var subjectId = commentService.getSubjectIdFromComment(id);
        return "redirect:/subjects/" + subjectId+"/";
    }


    @PostMapping("comment/delete/{idSubject}/{id}")
    public String deleteComment(@PathVariable Long idSubject,@PathVariable Long  id,Authentication authentication){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        commentService.deleteEntity(id, userDetails.getUser().getRole(),idSubject, userDetails.getUsername());
        return "redirect:/subjects/"+idSubject;
    }

    @RequestMapping(value="/comment/vote/{commentId}", method=RequestMethod.POST)
    public String upVote(@PathVariable Long commentId, Authentication authentication) {
        var user =(UserDetails) authentication.getPrincipal();
        var subjectId = commentService.getSubjectIdFromComment(commentId);
        var URL = "redirect:/subjects/" + subjectId+"/";
        commentService.upVote(user.getUserId(),commentId);
        return URL;
    }

    @RequestMapping(value="/comment/downVote/{commentId}", method=RequestMethod.POST)
    public String downVote(@PathVariable Long commentId, Authentication authentication) {
        var user =(UserDetails) authentication.getPrincipal();
        commentService.downVote(user.getUserId(),commentId);
        var subjectId = commentService.getSubjectIdFromComment(commentId);
        var URL = "redirect:/subjects/" + subjectId+"/";
        return URL;
    }

}
