package fr.uge.reddigm.controllers;

import fr.uge.reddigm.dao.SubjectDAO;
import fr.uge.reddigm.model.Role;
import fr.uge.reddigm.model.Subject;
import fr.uge.reddigm.model.UserDetails;
import fr.uge.reddigm.service.CategoryService;
import fr.uge.reddigm.service.SubjectService;
import fr.uge.reddigm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.sql.Timestamp;

@Controller
public class SubjectController {

    @Autowired
    private SubjectService subjectService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private UserService userService;


    @RequestMapping(value = "/createSubject", method = RequestMethod.GET)
    public String getForm(Model model, Principal principal) {
        model.addAttribute("username", principal.getName());
        model.addAttribute("subject", new SubjectDAO());
        model.addAttribute("categories", categoryService.retrieveCategoriesOrdered());
        return "create-subject";
    }

    @RequestMapping(value = "/createSubject", method = RequestMethod.POST)
    public String processForm(@RequestParam(value = "category") String category, @Valid @ModelAttribute("subject") SubjectDAO dao, BindingResult bindingResult, Principal principal,Model model) {
        dao.setCategory(category);

        if (bindingResult.hasErrors()) {
            model.addAttribute("username", principal.getName());
            model.addAttribute("subject", new SubjectDAO());
            model.addAttribute("categories", categoryService.retrieveCategoriesOrdered());
            return "create-subject";
        }

        dao.setTime(new Timestamp(System.currentTimeMillis()));
        subjectService.addEntity(dao);

        return "redirect:/";
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public String getCategories(Model model) {
        model.addAttribute("categories", categoryService.retrieveCategoriesOrdered());
        return "redirect:/"; //create browse-categories
    }

    @GetMapping(value = "/subject/{id}")
    public String subjectById(@PathVariable("id") Long id){
        return "redirect:/subjects/"+id;
    }



    @GetMapping(value = "/subjects/{id}")
    public String displaySubjectById(Model model, @PathVariable("id") Long id, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var optionalSubject = subjectService.retrieveSubjectById(id);
        if (optionalSubject.isEmpty()) {
            model.addAttribute("message", "The subject you are looking for was not found.");
            model.addAttribute("code", 404);
            return "error";
        }
        var subject = optionalSubject.get();
        var comments = subjectService.retrieveCommentsOfSubject(id);
        var photo = userService.findUser(subject.getAuthor()).getPhoto();
        model.addAttribute("subject", subject);
        model.addAttribute("mycomment", comments);
        model.addAttribute("photoProfil", photo);
        model.addAttribute("admin", userDetails.getUser().getRole().equals(Role.ROLE_ADMIN));
        return "subject";
    }


    @PostMapping("/admin/deleteSubject/{id}")
    public String deleteSubject(@PathVariable Long id, Model model, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        boolean deleted = subjectService.deleteSubject(id, userDetails.getUser().getRole());

        model.addAttribute("delted", deleted);
        return "redirect:/";
    }

    @RequestMapping(value = "/vote/{subjectId}", method = RequestMethod.POST)
    public String upVote(@PathVariable Long subjectId, Authentication authentication) {
        var user = (UserDetails) authentication.getPrincipal();
        subjectService.upVote(user.getUserId(), subjectId);
        return "redirect:/subjects/" + subjectId+"/";
    }

    @RequestMapping(value = "/downVote/{subjectId}", method = RequestMethod.POST)
    public String downVote(@PathVariable Long subjectId, Authentication authentication) {
        var user = (UserDetails) authentication.getPrincipal();
        subjectService.downVote(user.getUserId(), subjectId);

        return "redirect:/subjects/" + subjectId+"/";
    }

    @GetMapping(value = "/edit/{id}")
    public String edit(Model model, @PathVariable("id") Long id, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var optionalSubject = subjectService.retrieveSubjectByIdForUpdate(id);
        if (optionalSubject.isEmpty()) {
            model.addAttribute("message", "The subject you are looking for was not found.");
            model.addAttribute("code", 404);
            return "error";
        }
        var subject = optionalSubject.get();
        if( subject.getAuthor().getId() != userDetails.getUserId()){
            model.addAttribute("message", "unauthorized edit");
            model.addAttribute("code", 404);
            return "error";
        }
        model.addAttribute("subject", subject);

        return "edit_subject";
    }


    @PostMapping(value = "/updateSubject/{id}")
    public String edit(@PathVariable("id") Long id,@Valid @ModelAttribute("subject") SubjectDAO dao) {
        var toUpdateOpt = subjectService.retrieveSubjectByIdForUpdate(id);
        if (toUpdateOpt.isEmpty()){
            return "redirect:/";
        }
        Subject toUpdate = toUpdateOpt.get();
        toUpdate.setContent(dao.getContent());
        toUpdate.setCategory(categoryService.retrieveByTitle(dao.getCategory()));
        subjectService.update(toUpdate);

        return "redirect:/subjects/" + toUpdate.getId()+"/";
    }
}
