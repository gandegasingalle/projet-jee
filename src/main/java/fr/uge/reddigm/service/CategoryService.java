package fr.uge.reddigm.service;

import fr.uge.reddigm.model.Category;
import fr.uge.reddigm.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

@Service
public class CategoryService{

    @Autowired
    private CategoryRepository categoryRepo;

    @Transactional
    public Category addEntity(@NotNull Category category) {

        var result = categoryRepo.findByTitle(category.getTitle());
        if (result.isPresent())
            return result.get();
        else{
            return categoryRepo.save(category);
        }
    }

    @Transactional
    public Category retrieveByTitle(@NotNull String title) {
        return categoryRepo.findByTitle(title).get();
    }



    public List<Category> retrieveCategoriesOrdered(){
        var lst =  categoryRepo.findAll();
        Collections.sort(lst, (c1, c2) -> {
            if (c1.getSubjects().size() > c2.getSubjects().size()) return -1;
            if (c1.getSubjects().size() < c2.getSubjects().size()) return 1;
            return 0;
        });
        return lst;
    }

}
