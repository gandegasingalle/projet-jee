package fr.uge.reddigm.service;

import fr.uge.reddigm.dao.CommentDAO;
import fr.uge.reddigm.model.Comment;
import fr.uge.reddigm.model.Role;
import fr.uge.reddigm.repository.CommentRepository;
import fr.uge.reddigm.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private VotableService votableService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private UserService userService;

    @Autowired
    private SubjectRepository subjectRepository;


    @Transactional
    public Comment addEntity(@NotNull CommentDAO commentDAO) {
        var contentOptional = votableService.retrieveContentById(commentDAO.getContentId());
        if (contentOptional.isEmpty()) {
            throw new IllegalStateException("No such subject or comment  with this id " + commentDAO.getContentId());
        }
        var content = contentOptional.get();
        var comment = new Comment(userService.getUserWithLogin(commentDAO.getAuthor()), SubjectService.editURL(commentDAO.getContent()), content);
        return commentRepository.save(comment);
    }

    @Transactional
    public boolean deleteEntity(Long id, Role role, Long subjectId, String login) {
        var subjectOptional = subjectService.retrieveSubjectById(subjectId);
        var optionalComment = commentRepository.findByIdWithCommentsAndAuthor(id);
        var votableId = optionalComment.get().getVotable().getId();
        var optionalParent = commentRepository.findByIdWithCommentsAndAuthor(votableId);

        if (optionalComment.isEmpty() || subjectOptional.isEmpty() ||
                (!role.equals(Role.ROLE_ADMIN) && !optionalComment.get().getAuthor().getLogin().equals(login))) {
            return false;
        }
        Comment comment = optionalComment.get();

        if (!optionalParent.isEmpty()) {
            var parent = optionalParent.get();
            parent.getComments().remove(comment);
            commentRepository.save(parent);
        } else {
            var parent = subjectRepository.findSubjectWithComments(votableId).get();
            parent.getCommentsT().remove(comment);
            subjectRepository.save(parent);
        }
        return true;
    }


    /// COMMENTS ///

    @Transactional
    public Optional<Comment> addResponseToComment(@NotNull CommentDAO commentDAO) {
        var commentOptional = commentRepository.findByIdWithComments(commentDAO.getContentId());

        if (commentOptional.isPresent()) {
            var comment = commentOptional.get();
            var response = new Comment(userService.getUserWithLogin(commentDAO.getAuthor()), commentDAO.getContent(), commentOptional.get());
            comment.addComment(response);
            ;
            return Optional.of(commentRepository.save(comment));
        }
        return Optional.empty();
    }

    @Transactional
    public Optional<Comment> retrieveComment(long id) {
        return commentRepository.findById(id);
    }

    @Transactional
    public long getSubjectIdFromComment(long id) {
        var subjectOptional = subjectService.retrieveSubjectById(id);
        if (subjectOptional.isPresent()) {
            return subjectOptional.get().getId();
        }
        var content = commentRepository.findById(id).get().getVotable();
        return getSubjectIdFromComment(content.getId());

    }


    @Transactional
    public Optional<Comment> retrieveCommentWithWithCommentsAndAuthor(long id) {
        return commentRepository.findByIdWithCommentsAndAuthor(id);
    }

    @Transactional
    public Optional<Comment> update(long id, CommentDAO commentDAO) {
        Optional<Comment> commentOptional = commentRepository.findById(id);
        if (commentOptional.isEmpty()) {
            return Optional.empty();
        }
        Comment comment = commentOptional.get();
        if (!commentDAO.getAuthor().equals(comment.getAuthor().getLogin())) {
            return Optional.empty();
        }
        String newContent = commentDAO.getContent();
        comment.setContent(newContent);
        return Optional.of(commentRepository.save(comment));
    }

    /// VOTES ///

    public void downVote(Long userId, Long subjectId) { votableService.downVote(userId, subjectId); }

    public void upVote(Long userId, Long subjectId) {
        votableService.upVote(userId, subjectId);
    }


    @Transactional
    public int getNbVotes(Long commentId) throws IllegalStateException {
        var comment = commentRepository.findById(commentId);
        if (comment.isPresent()) {
            return comment.get().getTotalVote();
        }
        throw new IllegalStateException();
    }

}
