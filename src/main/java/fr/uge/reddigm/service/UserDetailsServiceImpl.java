package fr.uge.reddigm.service;

import fr.uge.reddigm.model.User;
import fr.uge.reddigm.repository.UserRepository;
import fr.uge.reddigm.securingweb.LoginAttemptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LoginAttemptService loginAttemptService;

    @Autowired
    private HttpServletRequest request;


    private String getClientIP() {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null){
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }

    public Boolean isBlocked(){
        String ip = getClientIP();
        return loginAttemptService.isBlocked(ip);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (isBlocked()) {
            throw new RuntimeException("blocked");
        }
        User user = userRepository.findByLoginEquals(username).get();

        if (user == null)
            throw new UsernameNotFoundException("User doesn't exist");

        return new fr.uge.reddigm.model.UserDetails(user);
    }


}
