package fr.uge.reddigm.service;

import fr.uge.reddigm.Utils.VoteType;
import fr.uge.reddigm.model.Votable;
import fr.uge.reddigm.repository.VoteRepository;
import fr.uge.reddigm.retry.VotableServiceWithFailure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VotableService {
    public final static Comparator<Votable> timeComparator = (o1, o2) -> o1.getTime().before(o2.getTime()) ? 1 : -1;
    public final static Comparator<Votable> voteComparator = (o1, o2)  -> o1.getTotalVote() < o2.getTotalVote() ? 1 : -1;

    @Autowired
    VotableServiceWithFailure votableServiceWithFailure;

    @Autowired
    private VoteRepository voteRepo;

    public void downVote(Long userId, Long subjectId) {
        makeVote(userId,subjectId, VoteType.Down);
    }
    public void upVote(Long userId, Long subjectId) {
        makeVote(userId,subjectId,VoteType.Up);
    }


    private void makeVote(Long userId, Long subjectId, VoteType voteType) {
        var retry = true;
        while (retry) {
            retry = false;
            try {
                votableServiceWithFailure.addVoteWrong(userId, subjectId, voteType);
            } catch (ObjectOptimisticLockingFailureException e) {
                retry = true;
            }
        }
    }

    @Transactional
    public Optional<Votable> retrieveContentById(Long id) {return voteRepo.findContentWithId(id);}


    public static List<Votable> ordinate(Collection<? extends Votable> votables, Comparator<Votable> comparator){
        return votables.stream().sorted(comparator).collect(Collectors.toList());
    }



}
