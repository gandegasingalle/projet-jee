package fr.uge.reddigm.service;

import fr.uge.reddigm.dao.SubjectDAO;
import fr.uge.reddigm.model.*;
import fr.uge.reddigm.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import fr.uge.reddigm.retry.VotableServiceWithFailure;

import java.util.*;

import static fr.uge.reddigm.service.VotableService.ordinate;
import static fr.uge.reddigm.service.VotableService.voteComparator;

@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepo;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private UserService userService;
    @Autowired
    private VotableService votableService;
    @Autowired
    VotableServiceWithFailure votableServiceWithFailure;

    @Transactional
    public Subject addEntity(@NotNull SubjectDAO dao) {
        var category = categoryService.retrieveByTitle(dao.getCategory());
        var user = userService.findUser(dao.getAuthor());
        var subject = new Subject(dao.getTitle(), editURL(dao.getContent()), user, categoryService.addEntity(category), null, dao.getTime());
        return subjectRepo.save(subject);
    }

    @Transactional
    public List<Subject> retrieveSubjects(){
        return subjectRepo.findAll();
    }

    @Transactional
    public Optional<Subject> retrieveSubjectById(Long id) {return subjectRepo.findSubjectWithId(id);}
    @Transactional
    public Optional<Subject> retrieveSubjectByIdForUpdate(Long id) {return subjectRepo.retrieveSubjectByIdForUpdate(id);}



    public void downVote(Long userId, Long subjectId) {
        votableService.downVote(userId,subjectId);    }
    public void upVote(Long userId, Long subjectId) {
        votableService.upVote(userId,subjectId);
    }


    @Transactional
    public int getNbVotes(Long subjectId) throws IllegalStateException {
        var subject = subjectRepo.findById(subjectId);
        if (subject.isPresent()) {
            return subject.get().getTotalVote();
        }
        throw new IllegalStateException();
    }
    @Transactional
    public List<Votable> retrieveCommentsOfSubject(Long id) {
        return ordinate(subjectRepo.findSubjectWithComments(id).get().getCommentsT(),voteComparator);
    }


    @Transactional
    public boolean deleteSubject(long id, Role role) {
        if (role.equals(Role.ROLE_ADMIN)) {
            subjectRepo.deleteById(id);
            return true;
        }
        return false;
    }

    public Page<Subject> findPaginated(Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Votable> subjects = ordinate(retrieveSubjects(), voteComparator);
        List<? extends Votable> list;

        if (subjects.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, subjects.size());
            list = subjects.subList(startItem, toIndex);
        }

        Page<Subject> subjectPage = new PageImpl<Subject>((List<Subject>) list, PageRequest.of(currentPage, pageSize), subjects.size());

        return subjectPage;
    }


    public static String editURL(String content) {
        StringBuilder stringBuilder= new StringBuilder();
        for (String word : content.split(" ")) {
            if (word.matches("href=\"([^\"]*)\"")) {
                word = word.replaceAll("href=\"([^\"]*)\"","href=/to?url="+word.replaceFirst("href=",""));
            }
            stringBuilder.append(word).append(" ");
        }
        return stringBuilder.toString();
    }

    public void update(Subject subject) {
        subjectRepo.save(subject);
    }
}
