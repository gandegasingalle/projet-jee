package fr.uge.reddigm.service;

import fr.uge.reddigm.model.*;
import fr.uge.reddigm.exception.FileException;
import fr.uge.reddigm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;

import static fr.uge.reddigm.service.VotableService.ordinate;
import static fr.uge.reddigm.service.VotableService.timeComparator;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private EmailService emailService;

    @Transactional
    public User addEntity(@NotNull User user) {
        var result = userRepository.findByLoginEquals(user.getLogin()).orElse(null);
        if (result != null)
            return result;

        if (user.getRole() == null) {
            user.setRole(Role.ROLE_USER);
        }
        try {
            user.setPhoto(Files.readAllBytes(Paths.get("img.png")));
        } catch (IOException e) {
            throw new FileException("internal problem loading the file image");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);

    }

    public Boolean checkIfUserExist(User user) {
        return userRepository.findById(user.getId()).isPresent();
    }

    public User findUser(User user) {
        return userRepository.findById(user.getId()).get();
    }

    public User findUser(String username) {
        return userRepository.findByLoginEquals(username).get();
    }

    @Transactional
    public User updatePasswordWithUser(User user, String newPassword) {
        user.setPassword(passwordEncoder.encode(newPassword));
        emailService.sendTestMessage();
        return userRepository.save(user);
    }

    public boolean matchPassword(String userPassword, String oldPassword) {
        return passwordEncoder.matches(oldPassword, userPassword);
    }

    private String generateRandomPassword() {
        int len = 10;
        int randNumOrigin = 48, randNumBound = 122;
        SecureRandom random = new SecureRandom();
        return random.ints(randNumOrigin, randNumBound + 1)
                .filter(i -> Character.isAlphabetic(i) || Character.isDigit(i))
                .limit(len)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString();
    }

    @Transactional
    public boolean checkAndSendRandomPassword(String email) {
        User user = userRepository.findByEmail(email).get();
        if (user == null) {
            return false;
        }
        var randomPassword = generateRandomPassword();
        var encodedRandomPassword = passwordEncoder.encode(randomPassword);
        user.setPassword(encodedRandomPassword);
        String emailContent = "This is your new password :\n" + randomPassword;
        emailService.sendSimpleMessage(user.getEmail(), "Recover your password", emailContent);
        return true;
    }

    @Transactional
    public User getUserWithLogin(String login) {
        return userRepository.findByLoginEquals(login).get();
    }

    @Transactional
    public void updateProfileImage(String login, byte[] image) {
        User user = userRepository.findByLoginEquals(login).get();
        user.setPhoto(image);
        userRepository.save(user);
    }

    @Transactional
    public User updateProfileEmail(String login, String email) {
        User user = userRepository.findByLoginEquals(login).get();
        user.setEmail(email);
        return userRepository.save(user);
    }


    @Transactional
    public List<Votable> getUserSubjects(Long userId) {
        return ordinate(userRepository.findUserSubjects(userId), timeComparator);
    }

    @Transactional
    public List<Votable> getUserComments(Long userId) {
        return ordinate(userRepository.findUserComments(userId), timeComparator);
    }

    @Transactional
    public Optional<String> checkLoginAndEmailExists(User user) {
        if (userRepository.findByEmail(user.getEmail()) != null) {
            return Optional.of("email already exists, please choose another one");
        }
        if (userRepository.findByLoginEquals(user.getLogin()) != null) {
            return Optional.of("login already exists, please choose another one");
        }
        return Optional.empty();
    }


}
