package fr.uge.reddigm;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReddigmApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReddigmApplication.class, args);
	}


}
