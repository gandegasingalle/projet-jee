package fr.uge.reddigm.exception;

public class FileException extends RuntimeException {
    public FileException(String errorMessage) {
        super(errorMessage);
    }
}
