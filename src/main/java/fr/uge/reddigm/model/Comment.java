package fr.uge.reddigm.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "comments")
@PrimaryKeyJoinColumn(name = "id")
public class Comment extends Votable{

    @OneToMany(fetch= FetchType.EAGER ,cascade = CascadeType.ALL, orphanRemoval = true) //cascade all voulu
    private Set<Comment> comments;

    @ManyToOne(fetch = FetchType.LAZY)
    private Votable votable;
    private final Timestamp time = new Timestamp(System.currentTimeMillis());


    public Comment(User author,String content, Set<Comment> comments, Votable votable) {
        super(content,author);
        this.comments = comments;
        this.votable = votable;
    }

    public Comment(User author,String content) {
        super(content,author);
    }

    public Comment() {}
    @Override
    public Timestamp getTime() {
        return time;
    }

    public Comment(User author, String content, Votable votable) {
        this(author, content, new HashSet<>(), votable);
    }

    public void setSubject(Votable votable) {
        this.votable = votable;
    }

    public void setComments(Set<Comment> commentSet) {
        this.comments = commentSet;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void addComment(Comment comment){
        comments.add(comment);
    }


    public Votable getVotable() {
        return votable;
    }

    public void setVotable(Votable votable) {
        this.votable = votable;
    }

}
