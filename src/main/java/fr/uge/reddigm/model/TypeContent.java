package fr.uge.reddigm.model;

public enum TypeContent {
    SUBJECT,
    COMMENT
}
