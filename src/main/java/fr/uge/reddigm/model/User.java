package fr.uge.reddigm.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique=true)
    private String login;
    @Pattern(regexp = ".{5,}",message = "please respect the pattern of the password")
    private String password;
    @Enumerated(EnumType.ORDINAL)
    private Role role;
    @Email
    @Column(unique=true)
    @Pattern(regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$",message = "please respect the pattern of the email")
    private String email;
    @OneToMany(cascade = {CascadeType.REMOVE}, mappedBy = "author", orphanRemoval=true)
    private Set<Comment> comments;
    @OneToMany(cascade = {CascadeType.REMOVE}, mappedBy = "author", orphanRemoval=true)
    private Set<Subject> subjects;
    @Lob
    private byte[] photo;

    public User(){}



    public User(String login, String password, Role role, String email) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.email = email;
        this.comments = new HashSet<>();
        this.subjects = new HashSet<>();
    }

    public User(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPhoto() {
        return Base64.getEncoder().encodeToString(photo);
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;
        if (id == null) return false;
        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : super.hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", email='" + email + '\'' +
                ", comments=" + comments +
                ", subjects=" + subjects +
                '}';
    }
}