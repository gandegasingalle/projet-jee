package fr.uge.reddigm.model;

import fr.uge.reddigm.Utils.VoteType;

import javax.persistence.*;

@Entity
@Table(name = "votes")
public class Vote{
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Votable votable;

    @Enumerated(EnumType.ORDINAL)
    private VoteType type;

    public Vote() {}

    public Vote(User user, Votable votable, VoteType type) {
        this.user = user;
        this.votable = votable;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Votable getVotable() {
        return votable;
    }

    public void setVotable(Votable votable) {
        this.votable = votable;
    }

    public VoteType getType() {
        return type;
    }

    public void setType(VoteType type) {
        this.type = type;
    }
}
