package fr.uge.reddigm.model;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}
