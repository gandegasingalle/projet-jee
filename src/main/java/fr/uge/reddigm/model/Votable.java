package fr.uge.reddigm.model;

import fr.uge.reddigm.Utils.VoteType;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Votable {
    @Id
    @GeneratedValue
    private Long id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "votable")
    private Set<Vote> votes;
    @Lob
    private String content;
    @ManyToOne
    private User author;
    int totalVote = 0;
    @Version
    private Long version;


    public Votable(){votes = new HashSet<>();}
    public Votable(  String content, User author) {
        this.content = content;
        this.author = author;
        votes = new HashSet<>();
    }



    public Long getId() {
        return id;
    }

    public void addVote(Vote v){
        Objects.requireNonNull(v);
        this.votes.add(v);
        v.setVotable(this);
        if(v.getType() == VoteType.Up){
            totalVote++;
        }else{
            totalVote--;
        }
    }
    public void removeVote(Vote vote){
        Objects.requireNonNull(vote);
        this.votes.remove(vote);
        vote.setVotable(null);
        if(vote.getType() == VoteType.Up){
            totalVote--;
        }else{
            totalVote++;
        }
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTotalVote() {
        return totalVote;
    }

    public abstract Timestamp getTime();



}
