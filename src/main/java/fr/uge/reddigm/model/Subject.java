package fr.uge.reddigm.model;



import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;
import java.util.*;

@Entity
@Table(name = "subjects")
@PrimaryKeyJoinColumn(name = "id")
public class Subject extends Votable{

    @NotEmpty
    private String title;
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REMOVE}, mappedBy = "votable",orphanRemoval = true)
    private Set<Comment> comments;
    private Timestamp time;

    public Subject(String title,  @NotEmpty String content, User author, Category category, Set<Comment> comments, Timestamp time) {
        super(content, author);
        this.title = title;
        this.category = category;
        this.comments = comments;
        this.time = time;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Subject() {
    }


    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Comment> getCommentsT() {
        return comments;
    }

    public void setCommentsT(Set<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }


    public void setTotalVote(int totalVote) {
        this.totalVote = totalVote;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + getId() +
                ", title='" + getTitle() + '\'' +
                ", content='" + getContent() + '\'' +
                ", author=" + getAuthor() +
                ", category=" + category +
                ", comments=" + comments +
                '}';
    }


}
