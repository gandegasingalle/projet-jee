﻿
<p align="center">
   logo
    <p align="center">ReddIGM is a network of communities where people can dive into their interests, hobbies and passions.</p>
</p>

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#description">Description</a>
      <ul>
        <li><a href="#developed-with">Developed with</a></li>
        <li><a href="#architecture">Architecture</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#requirements">Requirements</a></li>
        <li><a href="#clone-the-repository">Clone the Repository</a></li>
        <li><a href="#running-the-back-end-application-locally">Running the Back-End application locally</a></li>
        <li><a href="#use-application">Use application</a></li>
      </ul>
    </li>
    <li>
      <a href="#developers">Developers</a>
    </li>
    <li>
      <a href="#acknowledgments">Acknowledgments</a>
    </li>
  </ol>
</details>

## About the project
Reddigm is the best network of communities based on people's interests. Find communities you're interested in, and become part of an online community!
This project was developed during the last year of software master's degree at IGM.

## Description
<p align="center">
    logo
</p>
ReddIGM allows users to create subjects and posts about a specific topic.
Users can also comment or vote on these posts or other comments.
The site offers them the possibility of personalizing their profile.

### Developed with
* [Java](https://docs.oracle.com/en/java/javase/13/docs/api/index.html)
* [Spring-Boot](https://spring.io/projects/spring-boot)

### Architecture
This application follows a REST architecture, here is the technical stack used for this project :

<p align="center">
    schema
</p>

## Getting Started

### Requirements
For building and running the application you need, for Back-End application:

* [JDK 13](https://www.oracle.com/java/technologies/javase/jdk13-archive-downloads.html)
* [Maven 3](https://maven.apache.org/)

### Clone the Repository
As usual, you get started by cloning the project to your local machine:
```
git clone https://gitlab.com/gandegasingalle/projet-jee
```

### Running the Back-End application locally
There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `src/main/java/fr/uge/jee/web/service/reddIGM` ReddIgmApplication class from your IDE.


### Use application
You can then go on website which is available locally at: `http://localhost:8080/home`

## Developers

 - OUKSILI Smail
 - GANDEGA Singalle
 - HAROUCHE Djahida
 - RAMEAU Yohan
 - BOUAZZA Mehdi

## Acknowledgments

Inspiration.
* [Reddit](https://www.reddit.com/)
